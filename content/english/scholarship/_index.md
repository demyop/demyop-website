---
title: "Scholarship"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# image
image: "images/about/about-page.jpg"
# meta description
description : "Scholarship."
---

## WORRIED ABOUT FEES?

DeMyOp also provides Scholarship opportunities for the deserving candidates. So, if you’re worried about fees! Don’t be. We have everything sorted. All you need to do is prove your passion and your dedication by scoring the highest mark in tests conducted monthly. If you meet the above criteria, we are here to exempt you from paying the next month’s fee.

Good Luck! 

