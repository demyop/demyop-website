---
title: "About Us"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# about image
image: ""
# meta description
description : "Trying to connect the dots stands for Developing My Own Place. A holistic coaching initiative by Debasish Duarah, DeMyOp focuses on the overall development of students. Based in Sarupathar of Golaghat district in Assam, we emphasize on providing exposure to innumerable career opportunities both academics and other professional occupations."
---
## ABOUT

*“An investment in knowledge pays the best interest”* - Benjamin Franklin

### Who we are?

**DeMyOp** - Trying to connect the dots stands for Developing My Own Place. A holistic coaching initiative by Debasish Duarah, DeMyOp focuses on the overall development of students. Based in Sarupathar of Golaghat district in Assam, we emphasize on providing exposure to innumerable career opportunities both academics and other professional occupations. 

### What we offer?

DeMyOp offers a wide range of industry-oriented courses with a strong objective to deliver high-grade training to land your dream career options. Keeping in mind today’s tech-savvy world it has come up with the best industry-oriented courses for at a reasonable rate with limited seats promising best education to serve the ones who are looking for a change and want to make a mark in the market.

The approach of  DeMyOp breaks the conventional way of teaching and makes the students excel not only academically but also train them to meet the personality traits. 
DeMyOp provides tuition classes from class VI to IX and follows the NCERT syllabus. The subject covered are  Mathematics, Science and EVS. 

A fit body determines a fit mind ensuring a productive life. Sports not only helps in maintaining a healthy lifestyle but also gives a purpose to live. At DeMyOp, we provide football coaching every weekend. With the advent of technology and more use of electronic gadgets, the need for physical activity is a must. We fulfil the void. Apart from academics and sports, we also give importance to personality development, social awareness and entrepreneurship. 

### Our Mission

Our sole mission is to provide best quality education as well as to make aware of the importance of co-curricular activities and help in enhancing personality skills for the long run.

To provide the aspirants with the required knowledge and practical experience which would help them climb their scale of success. With the best industry-oriented courses we provide our students with the best study materials to maintain the top industry standards.

If you choose to study with us, we assure you:

✅ Quality education

✅ Practical implementation of the education

✅ Excellent Time management skills

✅ Personality Development

✅ Company-specific training

✅ 100+ practice programs

✅ 12+ hours of rigorous training a week

✅ Personal mentoring

✅ Certificates accepted worldwide

✅ Free Counselling

