
---
title: "Photography"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# about image
image: ""
# meta description
description : "Photography."
---

## Photography

Our photography range covers  Events, Marriages and Pre-wedding. The trainer of the photography team is Nilutpol Borah, a professional photographer who is the owner of Indipix. The charges are done based on the events. 

The package ranges from:

🔹 **1 DAY: Rs 12000 with Album**

🔹 **2 DAY: Rs 23000 with Album**

🔹 **3 DAY: Rs 33000 with Album**

## Let's have a look

![Photo1](https://i.imgur.com/fatPFF0.jpg)

![Photo2](https://i.imgur.com/qvgrbq1.jpg)

![Photo3](https://i.imgur.com/SRfLUNn.jpg)

![Photo4](https://i.imgur.com/wCtWGDY.jpg)
