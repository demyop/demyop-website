---
title: "Science"
date: 2020-07-05T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Science, any system of knowledge that is concerned with the physical world and its phenomena and that entails unbiased observations and systematic experimentation. In general, a science involves a pursuit of knowledge covering general truths or the operations of fundamental laws."
# course thumbnail
image: "https://i.imgur.com/1cyet19.jpg"
# taxonomy
category: "Science"
# teacher
teacher: "Debasish Duarah"
# duration
duration : "Unlimited"
# weekly
weekly : "03 hours"
# course fee
fee : "INR 700 P/M"
# apply url
apply_url : "https://docs.google.com/forms/d/e/1FAIpQLSfajKRnZA5cySOMyWSR7lw22AvFvGhAEhJ1S5AysT2wGzytUA/viewform"
# type
type: "course"
---


### About Course

Science, any system of knowledge that is concerned with the physical world and its phenomena and that entails unbiased observations and systematic experimentation. In general, a science involves a pursuit of knowledge covering general truths or the operations of fundamental laws.</p>

### Requirements

* Applicant must be enrolled in any school.
* Applicant must be a student of class IV to class IX.

### How to Apply

* Click on the apply button.
* Fill the form duly.
* We will contact you after the completion of form fillup.

### Fees and Funding

We will let you know about the fee structure after the completion of all formalities. You can pay fees in either online or offline mode.
