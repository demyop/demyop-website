---
title: "Football"
date: 2020-07-05T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "This course provides you a full on training on Football."
# course thumbnail
image: "https://i.imgur.com/t7acXz8.jpg"
# taxonomy
category: "Sports"
# teacher
teacher: "Anjan Gohain"
# duration
duration : "06 Month"
# weekly
weekly : "2 days"
# course fee
fee : "From: INR 500 P/M"
# apply url
apply_url : "https://docs.google.com/forms/d/e/1FAIpQLSfajKRnZA5cySOMyWSR7lw22AvFvGhAEhJ1S5AysT2wGzytUA/viewform"
# type
type: "course"
---


### About Course

This course provides you a full on training on Football.

## Schedule

| Schedule |    Saturday    |     Sunday     |
|:--------:|:--------------:|:--------------:|
|  Morning |       N/A      | 6:30 - 7:30 AM |
|  Evening | 3:30 - 5:30 PM | 3:30 - 5:30 PM |

### Requirements

* No any special requirements.
* All are welcome.
* Play safe, stay fit.

### How to Apply

* Click on the apply button.
* Fill the form duly.
* We will contact you after the completion of form fillup.

### Fees and Funding

We will let you know about the fee structure after the completion of all formalities. You can pay fees in either online or offline mode.
