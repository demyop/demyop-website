
---
title: "Web Development"
date: 2020-07-05T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "This course is the right fit for you which would make you ready to understand the full stack, build a web app and prepare a phenomenal portfolio to kick start your Web Developer career. 
."
# course thumbnail
image: "https://i.imgur.com/d9PADUq.jpg"
# taxonomy
category: "Programming"
# teacher
teacher: "Debasish Duarah"
# duration
duration : "06 Month"
# weekly
weekly : "03 hours"
# course fee
fee : "From: INR 500 P/M"
# apply url
apply_url : "https://docs.google.com/forms/d/e/1FAIpQLSfajKRnZA5cySOMyWSR7lw22AvFvGhAEhJ1S5AysT2wGzytUA/viewform"
# type
type: "course"
---


## Website Development Course

If you’re a typical internet user, you must browse a variety of websites of eCommerce, entertainment, business, education on a regular basis. Have you ever wondered how these websites are built and how do they function? Well, then this course is the right fit for you which would make you ready to understand the full stack, build a web app and prepare a phenomenal portfolio to kick start your Web Developer career. 

## What you’ll learn 
✅ Understand the structure and functionality of the world wide web

✅ Create dynamic web pages using HTML, CSS and Javascript

✅ Create a beautiful, responsive landing page for a startup

✅ Create complex HTML forms with validations

✅ Create full-stack web applications from scratch

✅ Become an expert at Googling code questions
