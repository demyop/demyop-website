---
title: "Environmental Science"
date: 2020-07-05T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Environmental science is an interdisciplinary academic field that integrates physical, biological and information sciences to the study of the environment, and the solution of environmental problems. Environmental science emerged from the fields of natural history and medicine during the Enlightenment."
# course thumbnail
image: "https://i.imgur.com/Om37dwY.jpg"
# taxonomy
category: "Science"
# teacher
teacher: "Debasish Duarah"
# duration
duration : "06 Month"
# weekly
weekly : "03 hours"
# course fee
fee : "From: INR 500 P/M"
# apply url
apply_url : "https://docs.google.com/forms/d/e/1FAIpQLSfajKRnZA5cySOMyWSR7lw22AvFvGhAEhJ1S5AysT2wGzytUA/viewform"
# type
type: "course"
---


### About Course

Environmental science is an interdisciplinary academic field that integrates physical, biological and information sciences to the study of the environment, and the solution of environmental problems. Environmental science emerged from the fields of natural history and medicine during the Enlightenment. </p>

### Requirements

* Applicant must enrolled in any school.
* Applicant must be a student of class IV to class IX.

### How to Apply

* Click on the apply button.
* Fill the form duly.
* We will contact you after the completion of form fillup.

### Fees and Funding

We will let you know about the fee structure after the completion of all formalities. You can pay fees in either online or offline mode.
