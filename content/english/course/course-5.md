---
title: "Computer Science"
date: 2020-07-05T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Computer science is the study of computation and information. Computer science deals with theory of computation, algorithms, computational problems and the design of computer systems hardware, software and applications."
# course thumbnail
image: "https://i.imgur.com/RKnxxnG.png"
# taxonomy
category: "Programming"
# teacher
teacher: "Debasish Duarah"
# duration
duration : "06 Month"
# weekly
weekly : "03 hours"
# course fee
fee : "From: INR 500 P/M"
# apply url
apply_url : "https://docs.google.com/forms/d/e/1FAIpQLSfajKRnZA5cySOMyWSR7lw22AvFvGhAEhJ1S5AysT2wGzytUA/viewform"
# type
type: "course"
---


### About Course

If you’re looking for a career in IT which pays off a handsome salary and fulfil your technology fantasies? Well, then a course in Computer science shall serve you to fulfil all of it. With a diversity of options available, a student can choose from the varied specializations like networking, cloud computing, DBMS (Database Management System), IoT and programming languages like Python, Java, HTML, C++, C.

Keeping in mind the vibrant and young aspiring students and the requirements of the recruiting industries, DeMyOp has brought a comprehensive course curated for the beginners as well as for the advanced ones who want to refine their skills further.
</p>

### Courses
  
#### JAVA
 
Looking for a career path in software programming, then you must not give a second thought to pursue this Java course which already opens a wide range of industry credentials. Java is one of the most popular programming languages of choice for Android programming.
 
##### What you’ll learn
 
    * Learn about data types and the usage
    * Object-oriented programming
    * Conditionals and control flow
    * Arrays and Array list
    * Learn how to use loops to iterate through lists and repeat code
    * Methods and constructors
    * Get Experience in JDBC (Java Database Connectivity) and Junit framework
    * Enhance your interpersonal skills to speed up your recruitment chances in the market.
 
#### PYTHON

If you’re looking to land your career in the developing data science industry?  If yes, learning Python would make absolute justice to your career. Python is a highly popular programming language across the globe because of its zero-difficulty level on learning and execution. Boosts to make you a successful, independent developer.
 
##### What you’ll learn
    * Basics of Python
    * Syntax of the Python Programming language
    * Strings and console output in Python.
    * Usage of conditionals and control flow to create programs which generate different outcomes.
    * Learn “while” and “for” loops Python
    * Learn data structures, list comprehensions, list slicing, and lambda expressions.
    * Apply Python to a real-time experience
    * Enhance your interpersonal skills to speed up your recruitment chances in the market.
 
#### C++

If you have only basic computer knowledge, you can start learning this powerful and fast C++ Programming language. C++ acts as an extension to the modern basic C language. If you want to get close to your machine and access all of your computer's hardware then C++ is the language for you to pursue. Writing data types modules in C++ can be used to extend the capabilities of Python too. The journey of the course starts from an introductory level for the beginners to an advanced level for the advanced ones.
 
##### What you’ll learn
    * C++ language fundamentals
    * C++ syntax, library, complex numbers, friendly functions etc.
    * How to create functions in C++, iostream, destructor, operator overloading, and more.
    * C++ header files, pointers, standards and references
    * Learn advanced C++ mechanisms, C++ exceptions, C++ templates and more
    * Memory management and allocation
    * File processing
    * Learn General OPP concepts
    * Enhance your interpersonal skills to speed up your recruitment chances in the market.
 
#### C LANGUAGE

C language is the most popular programming language which can be used to develop mobile applications, game development and enterprise software. If you know the programming language it opens a variety of career options.
 
##### What you’ll learn
    * Basics of C language
    * Data types and variables
    * How to work with Boolean logic and conditional statements
    * Learn to call and create Methods
    * How to organize data and automate repetitive tasks with arrays and loops
    * Learn how to organize, secure your data with interfaces and inheritance
    * Work with data in C# using lists and LINQ queries
 
#### HTML

HTML can be called as the foundation of all the web Pages as it defines the structure of the page. If you’re thinking to create your own Web Page then HTML is the correct beginning step for your career. With HTML you can create amazing looking websites. It is one of the most beginner-friendly languages one can begin their career with.

##### What you’ll learn
    * Basics of HTML
    * Learn how to create Forms using HTML and integrate HTML5 integrations
    * Learn how to write clearer and more accessible HTML using Semantic HTML Tags
    * Get hands-on experience in live projects
 
#### NETWORKING

If you’re looking to pursue a career in IT Networking and fill up the shortage of Network Administrator roles then this is the right fit for you. In this course of Networking, you’ll learn from the fundamentals of modern networking technologies and protocols to an overview of the cloud and to the implementations practically. You’ll be responsible for troubleshooting and repairing the specific hardware and software devices. Moreover, you’ll have extensive knowledge of various operating systems like LINUX, WINDOWS etc.
 
##### What you’ll learn
    * Learn Basic Networking topics like LANs, WANs, Ethernet and many more
    * Solve network problems by troubleshooting
    * Learn about Wireless technologies like Wi-fi, Bluetooth
    * Learn the mechanism of how the Internet works
    * Learn about transport and application layers
    * Security for various network types, including basic computer forensics

#### DBMS (Database Management System)

If you’re looking for a career in database development, data warehousing, or business intelligence, as well as for the entire Data Warehousing for Business Intelligence specialization then DBMS is the right course for you to pursue. DBMS is actually a system software designed to store and retrieve data of the users’ maintaining appropriate security measures. In today's world be it a small or big organization it requires a database management system to organize, secure and analyze complex information. By learning this course you’ll get an overview of how computer applications impact organizational operations globally.
 
##### What you’ll learn
    * Explore the database and its technology with its impact on Organizations
    * Learn relational data model terminology, integrity rules
    * Create Tables using SQL
    * Basic Query Formulation with SQL
    * Learn how to create database designs for business requirements
    * Learn data modelling problems and completion of an ERD
    * Learn Schema conversion
 
#### IoT (Internet of Things)

If you ever wondered if your smartphone can control the operations of your room’s light or your fridge could send you a list of the items you need to buy. IoT is, therefore, a technology that takes the concept of internet connectivity beyond your computer to everyday items. If you want to dive into this career path, you’ve come to the right platform. This course will help you develop skills and experiences you can employ in designing novel systems.
 
##### What you’ll learn
    * Introduction to Internet of Things (IoT)
    * Explore various IoT Sensors and Devices
    * Learn about IoT networks and protocols
    * Learn application of software solutions for different systems and Big Data needs
    * Learn how to design a secure system
 
 
#### Cloud Computing

In today’s world, careers in Cloud Computing are in high demand in the IT Industry. Therefore, if you happen to pursue a career in this field, it will prove fruitful to you as it has a plethora of opportunities for IT Professionals. A career in this field requires complex coding and database management. Cloud computing is expected to become a $300 billion business by 2021 globally as Cloud is being adopted in increasing numbers for business worldwide.
 
##### What you’ll learn
    * Fundamentals of cloud computing
    * Types of cloud-like private, public, hybrid
    * Learn Cloud platforms like AWS, Azure and Google Cloud
    * Models of cloud computing like SaaS, LaaS, PaaS
    * Learn about Cloud concepts like OpenShift, Cloud-Native Development
    * Traditional IT and Cloud Computing


### Schedule

|  Schedule |         Saturday        |            Sunday           |
|:---------:|:-----------------------:|:---------------------------:|
| Afternoon | 2:00 - 3:00 PM (Theory) | 12:30 - 2:30 PM (Practical) |

### Requirements

* All are welcome.

### How to Apply

* Click on the apply button.
* Fill the form duly.
* We will contact you after the completion of form fillup.

### Fees and Funding

We will let you know about the fee structure after the completion of all formalities. You can pay fees in either online or offline mode.
