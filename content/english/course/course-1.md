---
title: "Mathematics"
date: 2020-07-05T15:27:17+06:00
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Mathematics is the science that deals with the logic of shape, quantity and arrangement. Math is all around us, in everything we do. It is the building block for everything in our daily lives, including mobile devices, architecture (ancient and modern), art, money, engineering, and even sports."
# course thumbnail
image: "https://i.imgur.com/nDgPROV.jpg"
# taxonomy
category: "Mathematics"
# teacher
teacher: "Debasish Duarah"
# duration
duration : "Unlimited"
# weekly
weekly : "03 hours"
# course fee
fee : "INR 800 P/M"
# apply url
apply_url : "https://docs.google.com/forms/d/e/1FAIpQLSfajKRnZA5cySOMyWSR7lw22AvFvGhAEhJ1S5AysT2wGzytUA/viewform"
# type
type: "course"
---


### About Course

Mathematics is the science that deals with the logic of shape, quantity and arrangement. Math is all around us, in everything we do. It is the building block for everything in our daily lives, including mobile devices, architecture (ancient and modern), art, money, engineering, and even sports.</p>

### Requirements

* Applicant must enrolled in any school.
* Applicant must be a student of class IV to class IX.

### How to Apply

* Click on the apply button.
* Fill the form duly.
* We will contact you after the completion of form fillup.

### Fees and Funding

We will let you know about the fee structure after the completion of all formalities. You can pay fees in either online or offline mode.
