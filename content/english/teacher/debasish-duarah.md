---
title: "Debasish Duarah"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Computer Science Faculty"
# teacher portrait
image: "https://i.imgur.com/7NoHlIZ.png"
# course
course: "Computer Science"
# biography
bio: "Software Engineer | B Tech CSE | M Tech CSE | Basketball National Player | SGFI"
# interest
interest: ["Computer Networking","Network Security","Internet of Things", "Cloud Computing"]
# contact info
contact:
  # contact item loop
  - name : "debasish.duarah@yahoo.com"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:debasish.duarah@yahoo.com"

  # contact item loop
  - name : "+91 99543 74636"
    icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
    link : "tel:+919954374636"

  # contact item loop
  - name : "Debasish Duarah"
    icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
    link : "https://www.facebook.com/debasishduarah"


# type
type: "teacher"
---

### Publication

* Anomaly Detection of IoT data using Machine learning Algorithm in International Conference on emerging current trends in computing and expert technology, Springer.
* Fuzzy Analytic hierarchy process over student expectation in current education system in National Conference.