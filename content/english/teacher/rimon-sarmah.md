---
title: "Rimon Sarmah"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Website Admin"
# teacher portrait
image: "https://i.imgur.com/blNz4jn.jpg"
# course
course: "Website Admin"
# biography
bio: "Programmer | Backend Developer"
# interest
interest: ["Computer Networking","Cyber Security","Web Developement", "Programming"]
# contact info
contact:
  # contact item loop
  - name : "rimonsarmahjnv@gmail.com"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:rimonsarmahjnv@gmail.com"

  # contact item loop
  - name : "+91 70028 28442"
    icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
    link : "tel:+917002828442"

  # contact item loop
  - name : "Rimon Sarmah"
    icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
    link : "https://www.facebook.com/rimon.sarmah.73"

  # contact item loop
  - name : "Rimon Sarmah"
    icon : "ti-twitter-alt" # icon pack : https://themify.me/themify-icons
    link : "https://twitter.com/sarmah_rimon"


# type
type: "teacher"
---
