---
title: "Anjan Gohain"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Football Coach"
# teacher portrait
image: "https://i.imgur.com/XEj1O1D.jpg"
# course
course: "Football"
# biography
bio: "NIS Certification | AFC C License | AFC GK Level 1 | Achievement: Recent - Assistant Coach/GK | Khelo India U17-U21 | Runners up boys and Girls."
# interest
interest: ["Football","Goal Keeping"]
# contact info
contact:

  # contact item loop
  - name : "+91 94012 95909"
    icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
    link : "tel:+919401295909"

  # contact item loop
  - name : "Anjan Gohain"
    icon : "ti-facebook" # icon pack : https://themify.me/themify-icons
    link : "https://www.facebook.com/anjan.gohain.94"


# type
type: "teacher"
---