---
title: "Jay Prakash Sureka"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "Chartered Accountant"
# teacher portrait
image: "https://i.imgur.com/AcSkUzk.jpg"
# course
course: "Mathematics"
# biography
bio: "Chartered Accountant"
# interest
interest: ["Mathematics"]
# contact info
contact:
  # contact item loop
  - name : "jayprakash.sureka@rediffmail.com"
    icon : "ti-email" # icon pack : https://themify.me/themify-icons
    link : "mailto:jayprakash.sureka@rediffmail.com"

  # contact item loop
  - name : "+91 96746 56033"
    icon : "ti-mobile" # icon pack : https://themify.me/themify-icons
    link : "tel:+919674656033"


# type
type: "teacher"
---
