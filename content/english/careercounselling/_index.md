---
title: "Career Counselling"
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# about image
image: ""
# meta description
description : "Demyop comes with a specialised free counselling service for all those who feel very uncertain of what to pursue after completing their Higher Secondary. What it takes for you to reach out to us for serving you with the best career opportunities based on your interests and passion."
---

## CAREER COUNSELLING

*“Which career option should I opt for? What am I passionate about?”* These are the most difficult questions that roll wild when a student goes for higher education.

For a student, the waves of the uncertainty of their future hit very hard and for many, it becomes very difficult to pursue any specified courses.

**DeMyOp** comes with a specialised free counselling service for all those who feel very uncertain of what to pursue after completing their Higher Secondary. What it takes for you to reach out to us for serving you with the best career opportunities based on your interests and passion.

## OUTCOMES OF COUNSELLING

🔹 Become aware of the various career opportunities 

🔹 Get a clear idea of where to study

🔹 Become aware of any important skills to develop

🔹 Get a glance of your profession

🔹 Steps to be taken further

## WE HELP YOU WITH:

### Career Assessment

We have personalised assessment tests to help you discover your interests and map it with the best career option available.

#### One to One Counselling

Get personal one-to-one, online and personal counselling actions which involve interactions with best industry experts to gain deeper insights in your field. Get a personal mentor to help you guide through your career journey.
You can come to us for the following career guidance.

🔹 Engineering

🔹 Medical

🔹 General

🔹 Physics

🔹 Maths

🔹 Chemistry

🔹 Biology

🔹 Hotel Management

🔹 Interior Designing

🔹 Textile Designing

🔹 NIFT

🔹 Photography

🔹 Anthropology

🔹 Sociology

🔹 CA

🔹 CMA

🔹 Sports

