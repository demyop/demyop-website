---
title: "Opening Ceremony"
# Schedule page publish date
publishDate: "2020-07-02T00:00:00Z"
# event date
date: "2020-08-15T15:27:17+06:00"
# post save as draft
draft: false
# page title background image
bg_image: "images/backgrounds/page-title.jpg"
# meta description
description : "We are deligted to let you know that we are going to have opening ceremony of DeMyOp on 15th August, 2020. Your presence is highly appreciated."
# Event image
image: "images/events/event-1.jpg"
# location
location: "Sarupathar, Golaghat"
# entry fee
fee: "Free of Cost"
# apply url
apply_url : "#"
# event speaker
speaker:
  # speaker loop
  - name : "Jack Mastio"
    image : "images/event-speakers/speaker-1.jpg"
    designation : "Teacher"

  # speaker loop
  - name : "John Doe"
    image : "images/event-speakers/speaker-2.jpg"
    designation : "Teacher"

  # speaker loop
  - name : "Randy Luis"
    image : "images/event-speakers/speaker-3.jpg"
    designation : "Teacher"

  # speaker loop
  - name : "Alfred Jin"
    image : "images/event-speakers/speaker-4.jpg"
    designation : "Teacher"

# type
type: "event"
---

### About Event

We are deligted to let you know that we are going to have opening ceremony of DeMyOp on 15th August, 2020. Your presence is highly appreciated